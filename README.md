Photoshop Layer Search Plugin
=============================

Photoshop does not support the search for layer function, and I know you feel sad about that. But with a help of plug ins, you can do it on your own laptop. Make use of these plug ins and make your Photoshop tasks easier. Start the easy steps [here](http://www.ethanolfireplacepros.com).

Download/Install
================

Download
--------

Search installer.


From source
-----------

    git clone git@github.com:markupwand/photoshop-layersearch.git
    cd photoshop-layersearch/
    rake

Requirements
============
Tested it with Adobe PS5, Mac.

Screenshot
===========
![Screenshot](https://www.evernote.com/shard/s8/sh/aaa1e2c9-003f-4eeb-acb2-bab10fcc998b/1acdfe47f7c3286da5f5fd632bf03c07/res/44bada78-bf3a-4ad3-8d8c-3e50f3dd10b1/Photoshop-20130110-142050.png.jpg?resizeSmall&width=832)


How to use
===
After opening a psd file in photoshop, Window->Extensions->LayerSearch